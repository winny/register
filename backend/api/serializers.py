from rest_framework import serializers, relations
from api.models import User, Product, Transaction


BASE_FIELDS = ('id', 'created', 'updated', 'deleted',)
BASE_READ_ONLY_FIELDS = BASE_FIELDS


class ProductSerializer(serializers.ModelSerializer):
    class Meta:
        model = Product
        fields = ('price', 'kind', 'description', ) + BASE_FIELDS
        read_only_fields = BASE_FIELDS




class TransactionSerializer(serializers.ModelSerializer):
    class Meta:
        model = Transaction
        fields = ('created', 'items', 'tendered', 'total', 'finalized', 'change', ) + BASE_FIELDS
        read_only_fields = ('created', 'total', 'finalized', 'change', ) + BASE_FIELDS

    total = serializers.SerializerMethodField()
    change = serializers.SerializerMethodField()
    finalized = serializers.SerializerMethodField()

    def get_total(self, obj):
        return obj.total

    def get_change(self, obj):
        return obj.change

    def get_finalized(self, obj):
        return obj.finalized

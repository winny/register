from django.db import models
from django.utils import timezone
from django.contrib.auth.models import AbstractUser
from django.db.models import Sum
from decimal import Decimal


#__all__ = ['User', 'Product', 'Transaction']

# Create your models here.
class AbstractModel(models.Model):
    """Abstract model for all API-accessible resources to inherit from

    All models should subclass this.

    Originally inspired by https://stackoverflow.com/a/1737078/2720026
    """
    created = models.DateTimeField(editable=False)
    updated = models.DateTimeField()
    deleted = models.DateTimeField(null=True)

    def save(self, *args, **kwargs):
        """On save, update timestamps."""
        if not self.id:
            self.created = timezone.now()
        self.updated = timezone.now()
        return super(AbstractModel, self).save(*args, **kwargs)

    class Meta:
        abstract = True


class User(AbstractUser, AbstractModel):
    """The main User model"""
    pass


class Product(AbstractModel):
    price = models.DecimalField(max_digits=10, decimal_places=2)

    PRODUCT_KIND_CHOICES = (
        ('immediate', 'Immediate product added from the cash register UI'),
        ('inventory', 'Product programemd into the inventory management system'),
    )
    kind = models.CharField(
        max_length=max(len(c[0]) for c in PRODUCT_KIND_CHOICES),
        choices=PRODUCT_KIND_CHOICES,
    )

    description = models.TextField(blank=True)


class Transaction(AbstractModel):
    items = models.ManyToManyField('Product')

    tendered = models.DecimalField(max_digits=10, decimal_places=2, default=0)

    creator = models.ManyToManyField('User')

    @property
    def change(self):
        return max(self.tendered - self.total, 0)

    @property
    def finalized(self):
        """Is the transaction complete?"""
        return self.tendered >= self.total

    @property
    def total(self):
        return self.items.aggregate(Sum('price'))['price__sum'] or Decimal(0)

from rest_framework import routers
from api.views import TransactionViewSet, ProductViewSet


router = routers.DefaultRouter()
router.register(r'product', ProductViewSet, basename='product')
router.register(r'transaction', TransactionViewSet, basename='transaction')

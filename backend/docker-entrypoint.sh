#!/bin/sh
set -eu

python3 ./manage.py migrate --noinput
python3 ./manage.py loaddata users  # Ensure users are configured
python3 ./manage.py runserver 0.0.0.0:8000

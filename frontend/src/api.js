const superagent = require('superagent')

const endpoint = process.env.api_endpoint

function login(username, password) {
  console.log('Attempting login against ' + endpoint);
  return superagent
    .post(endpoint + 'login/')
    .accept('json')
    .send({username: username, password: password})
}

function logout(token) {
  return superagent
    .post(endpoint + 'logout/')
    .accept('json')
    .set('Authorization', 'Token ' + token)
}

function ping(token) {
  return superagent
    .get(endpoint + 'ping/')
    .accept('json')
    .set('Authorization', 'Token ' + token)
}

function make_object(resource) {
  return {
    create: function(token, obj) {
      return superagent
        .post(endpoint + resource + '/')
        .accept('json')
        .set('Authorization', 'Token ' + token)
        .send(obj)
    },
    get: function(token, id) {
      return superagent
        .get(endpoint + resource + '/' + id.toString() + '/')
        .accept('json')
        .set('Authorization', 'Token ' + token)
    },
    list: function(token) {
      return superagent
        .get(endpoint + resource + '/')
        .accept('json')
        .set('Authorization', 'Token ' + token)
    },
    delete: function(token, id) {
      return superagent
        .delete(endpoint + resource + '/' + id.toString() + '/')
        .accept('json')
        .set('Authorization', 'Token ' + token)
    },
    update: function(token, id, obj) {
      return superagent
        .patch(endpoint + resource + '/' + id.toString() + '/')
        .accept('json')
        .set('Authorization', 'Token ' + token)
        .send(obj)
    }
  }
}

exports.transaction = make_object('transaction');
exports.product = make_object('product');
exports.login = login;
exports.logout = logout;
exports.ping = ping;
exports.endpoint = endpoint;

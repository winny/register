function cc(change, denominations = [20000, 10000, 500, 100, 25, 10, 5, 1]) {
  const d = denominations.slice().sort((a, b) => a - b).reverse()
//  console.log(d)
  const result = d.reduce(({change, ...acc}, denomination) => {
    console.log(JSON.stringify({change, ...acc}))
    console.log(JSON.stringify(denomination))
    const count = Math.floor(change / denomination)
    if (count > 0) {
      acc[denomination] = count;
    }
    acc.change = change % denomination
    console.log(JSON.stringify(acc))
    return acc
  }, {change:change})
  delete result.change
  result.denominations = d.filter(denom => denom.toString() in result)
  return result
}

exports.cc = cc;
exports.moneyinfo = {
  '1': {name:'pennies'},
  '5': {name:'nickles'},
  '10': {name:'dimes'},
  '25': {name:'quarters'},
  '100': {name:'dollars'},
  '500': {name:'five dollars'},
  '1000': {name:'ten dollars'},
  '2000': {name:'twenty dollars'}
}
